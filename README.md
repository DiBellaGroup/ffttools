# The FftTools Package

## Introduction

This package adds functions and classes that support a variety of Fast Fourier Transform alternatives which have proven useful in a medical image processing workflow.

This package contains several key components which are explained in the following sections.

### Version

The VERSION.mat file contains a single variable `VERSION` which contains a string with the version number of this package. No modifications to this file should be made without updating this number according to [semantic versioning best practices](http://semver.org/).

### Package Management

Running `FftTools.verify` will examine the environment that FftTools sees and ensure that any packages it depends on are available and of the correct version.

### Tests

To make sure this software works as expected in the present environment, tests have been provided and can be run by executing `FftTools.test`.

### Docs

In the `docs/` directory some markdown files are provided explaining how to use each of the functions and classes this package provides.
