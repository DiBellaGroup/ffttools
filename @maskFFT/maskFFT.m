classdef MaskFft
  properties
    adjoint = 0;
    maskSize
    xSize
    ySize
    zSize
    mask
  end
  methods
    function obj = MaskFft(mask)
      % Get original dimensions mask
      obj.maskSize = size(mask);
      % Put rest of dimensions into zSize
      [obj.xSize, obj.ySize, obj.zSize] = size(mask);
      obj.mask = reshape(mask, obj.xSize, obj.ySize, obj.zSize);
    end
  end
end
